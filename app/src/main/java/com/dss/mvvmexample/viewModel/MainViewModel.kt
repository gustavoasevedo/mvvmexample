package com.dss.mvvmexample.viewModel

import android.arch.lifecycle.*
import com.dss.mvvmexample.domain.NameDomain
import com.dss.mvvmexample.repositories.model.User


/**
 * Created by Gustavo Asevedo on 19/07/2018.
 */
class MainViewModel : ViewModel(), LifecycleObserver {
    private var user: MutableLiveData<User>? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun oncCreate() {
        getUser()
    }

    fun getUser(): LiveData<User> {
        if (user == null) {
            user = MutableLiveData()
            changeName()
        }
        return user as MutableLiveData<User>
    }

    fun changeName() {
        val newName = NameDomain.generateRandomString()

        user?.value = User(null,newName)
    }



}
