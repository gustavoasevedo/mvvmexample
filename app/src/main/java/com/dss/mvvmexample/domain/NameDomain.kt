package com.dss.mvvmexample.domain

import com.dss.mvvmexample.MVVMApplication
import com.dss.mvvmexample.repositories.dao.ApplicationDatabase
import com.dss.mvvmexample.repositories.model.User
import kotlinx.coroutines.experimental.launch
import java.nio.charset.Charset
import java.util.*

/**
 * Created by Gustavo Asevedo on 19/07/2018.
 */
abstract class NameDomain{

    companion object {
        fun generateRandomString(): String {
            val array = ByteArray(7)
            Random().nextBytes(array)
            val generatedString = String(array, Charset.forName("UTF-8"))

            storeNewUser(generatedString)

            return generatedString
        }

        private fun storeNewUser(name : String){
            val user = User(null,name)
            val result = launch {
                ApplicationDatabase.getInstance(MVVMApplication.context)?.UserDao()?.insert(user)
            }
        }
    }


}