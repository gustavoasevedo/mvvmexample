package com.dss.mvvmexample.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dss.mvvmexample.R
import com.dss.mvvmexample.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*




class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val model  = ViewModelProviders.of(this).get(MainViewModel::class.java)

        model.getUser().observe(this, Observer {
            txtTitle.text = model.getUser().value?.name
        })

        btnRandomText.setOnClickListener {

            model.changeName()
        }
    }
}
