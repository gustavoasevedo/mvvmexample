package com.dss.mvvmexample

import android.app.Application
import android.content.Context

/**
 * Created by Gustavo Asevedo on 19/07/2018.
 */
class MVVMApplication : Application() {

    override fun onCreate() {
        instance = this
        super.onCreate()
    }

    companion object {
        var instance: MVVMApplication? = null
            private set

        val context: Context
            get() = instance!!.applicationContext
    }
}
