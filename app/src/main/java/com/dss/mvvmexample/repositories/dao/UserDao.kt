package com.dss.mvvmexample.repositories.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.dss.mvvmexample.repositories.model.User

/**
 * Created by Gustavo Asevedo on 19/07/2018.
 */

@Dao
interface UserDao {
    @Insert()
    fun insert(user: User)

    @Query("SELECT * from User")
    fun getAll(): LiveData<List<User>>
}