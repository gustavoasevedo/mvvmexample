package com.dss.mvvmexample.repositories.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Gustavo Asevedo on 19/07/2018.
 */
@Entity(tableName = "User")
data class User(
        @PrimaryKey(autoGenerate = true) var id : Int?,
        var name : String
){
    constructor():this(null,"")
}