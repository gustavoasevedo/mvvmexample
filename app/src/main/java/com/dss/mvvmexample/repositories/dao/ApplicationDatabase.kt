package com.dss.mvvmexample.repositories.dao

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.dss.mvvmexample.repositories.model.User

/**
 * Created by Gustavo Asevedo on 19/07/2018.
 */

@Database(entities = arrayOf(User::class), version = 1)
abstract class ApplicationDatabase : RoomDatabase() {
    abstract fun UserDao() : UserDao

    companion object {
        private var instance : ApplicationDatabase? = null

        fun getInstance(context: Context): ApplicationDatabase? {
            if (instance == null) {
                synchronized(ApplicationDatabase::class) {
                    if (instance == null) {
                        instance = Room.databaseBuilder(context.applicationContext,
                                ApplicationDatabase::class.java!!, "ApplicationDatabase")
                                .build()
                    }
                }
            }
            return instance
        }

        fun destroyInstance() {
            instance = null
        }
    }
}